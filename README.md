# Jinkins CICD | Acceptance testing

[Please find the specifications by clicking](https://github.com/eazytraining/)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang)


_____



## Overview of lab

1. We will install the http_request plugin `(https://plugins.jenkins.io/http_request/)`.
2. We will use this plugin to set up the "acceptance test" job:
   - We will use the source [alpinehelloworld](https://github.com/CarlinFongang/alpinehelloworld.git) to specify the GitHub link of the project.
   - Build the image (without pushing it).
   - Start a container using the built image with the help of the previously installed plugin.
   - Use the plugin to verify that the container returns the HTTP status code 200.

## Prerequisites
Perform the Build phase.
[Find the instructions for setting up a Jenkins build here](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab2-build)

## Installation du plugin http_request
Dashboard > Administer Jenkins > Plugins > Available plugins > 
In the search bar, type: `http_request`
Check the checkbox and initiate the installation.
>![alt text](img/image.png)
>![alt text](img/image-1.png)

## Création du job "Test acceptance"
1. Setting up the job name
Dashboard > New Item | Enter "Test acceptance"
- Enable Free-style project and confirm
>![alt text](img/image-2.png)

2. Configuration of variables
Dashboard > Test acceptance > Configuration > General
>![alt text](img/image-3.png)

3. Configuration of the source code
Dashboard > Test acceptance > Configuration > Source code | Select Git
Set the branch, in our case, it's "master"
>![alt text](img/image-4.png)

4. Build Configuration
Dashboard > Test acceptance > Configuration > Build step > Add a build step | Script Shell
Configure the build scripts and launch the container of the alpinehelloworld application
>![alt text](img/image-5.png)

5. Configuration of the HTTP Request for the acceptance test
Dashbord > Test acceptance > Configuration > Build step > Add a build step > HTTP Request
>![alt text](img/image-6.png)
>![alt text](img/image-7.png)

6. Advance setting on HTTP Request
change Reponse code expected 100:399 > 200
>![alt text](img/image-8.png)

7. Cleaning up the test environment
Adding a shell script after launching and testing the Alpine container
`docker stop ${IMAGE_NAME} && docker rm ${IMAGE_NAME}`
>![alt text](img/image-9.png)

8. Requette
>![alt text](img/image-11.png)

9. Success of the HTTP_REQUEST test
>![alt text](img/image-10.png)

10. Result
>![alt text](img/image-12.png)





