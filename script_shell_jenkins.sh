#!/bin/bash
# build
docker stop ${IMAGE_NAME} && docker rm -f ${IMAGE_NAME}
docker build -t ${IMAGE_NAME}:${IMAGE_TAG} .
 
# Run contenair with image docker alpine for test
docker run -d -p 80:5000 -e PORT=5000 --name ${IMAGE_NAME} ${IMAGE_NAME}:${IMAGE_TAG}
sleep 5
CONTAINER_NAME=$(docker ps -a --format '{{.Names}}' --no-trunc | awk '{print $NF}' | sort | head -n 1)
ip_address=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_NAME)

#Clean environnement, après le test Http_Request
docker stop ${IMAGE_NAME} && docker rm -f ${IMAGE_NAME}
docker rmi ${IMAGE_NAME}